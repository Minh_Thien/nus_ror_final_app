$(document).ready ->
  imagesPreview = (input, placeToInsertImagePreview) ->
    if input.files
      filesAmount = input.files.length
      i = 0
      while i < filesAmount
        reader = new FileReader

        reader.onload = (event) ->
          $($.parseHTML('<img class="col-md-3 col-sm-4 col-6 p-1 m-0 img-thumbnail preview-album-image">')).attr('src', event.target.result).insertBefore placeToInsertImagePreview
          return

        reader.readAsDataURL input.files[i]
        i++
    return

  $('#gallery-photo-add').on 'change', ->
    imagesPreview this, 'div.gallery'
    return
  return


window.setTimeout (->
  $('.alert').fadeTo(500, 0).slideUp 500, ->
    $(this).remove()
    return
  return
), 5000