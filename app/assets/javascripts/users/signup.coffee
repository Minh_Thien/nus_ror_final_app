$ ->
  $(".new_user").validate {
    rules:
      'user[firstname]': {
        required: true
        maximum: 255
      },
      'user[lastname]': {
        required:true
        maximum: 255
      },
      'user[email]': {
        required:true,
        email: true
      },
      'user[password]': {
        required:true
        maximum: 64
      },
      'user[password_confirmation]': {
        required:true
        maximum: 64
      }
  }