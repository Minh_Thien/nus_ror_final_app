class Album < ApplicationRecord

  belongs_to :user
  has_many :photos, dependent: :destroy
  accepts_nested_attributes_for :photos, allow_destroy: true, reject_if: proc { |attributes| attributes['image'].blank? }
  scope :public_mode, -> { where(mode: "Public") }

  validates :title, :description, :mode, presence: true
  validates :title, length: { maximum: 140,
    too_long: "Maximum 140 characters long" }
  validates :description, length: { maximum: 300,
    too_long: "Maximum 300 characters long" }
end
