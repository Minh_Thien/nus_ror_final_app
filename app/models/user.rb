class User < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable

  has_many :albums, dependent: :destroy
  has_many :photos, dependent: :destroy

  validates :firstname, :lastname, :email, presence: true
  validates :firstname, length: { maximum: 25,
    too_long: "Maximum 25 characters long" }
  validates :lastname, length: { maximum: 25,
    too_long: "Maximum 25 characters long" }
  validates :email, uniqueness: true
  validates :email, length: { maximum: 255,
    too_long: "Maximum 255 characters long" }

end