class Photo < ApplicationRecord
  mount_uploader :image, ImageUploader

  belongs_to :user
  belongs_to :album, optional: true
  scope :public_mode, -> { where(mode: "Public") }
  validates :title, :description, :mode, presence: true
  validates :title, length: { maximum: 140,
    too_long: "Maximum 140 characters long" }
  validates :description, length: { maximum: 300,
    too_long: "Maximum 300 characters long" }
end
