class FeedsController < ApplicationController

  def index
    @photos = Photo.public_mode.where.not(title:"Image created from album!")
    @albums = Album.public_mode
    @feeds = (@photos + @albums).sort_by {|record| record.updated_at}.reverse
  end

end