class PhotosController < ApplicationController

  def index
    if !current_user.admin
      @photos = current_user.photos.where.not("photos.title": "Image created from album!").page(params[:page])
    else
      @photos = Photo.where.not("photos.title": "Image created from album!").page(params[:page])
    end
  end

  def new
    @photo = Photo.new
  end

  def edit
    @photo = Photo.find(params[:id])
  end

  def create
    @photo = Photo.new(photo_params)
    @photo.user = current_user
    if @photo.save
      redirect_to photos_path
    else
      render :new
    end
  end

  def show
    @photo = Photo.find(params[:id])
  end

  def update
    @photo = Photo.find(params[:id])
    if @photo.update(photo_params)
      redirect_to photos_path
    else
      render 'edit'
    end
  end

  def destroy
    @photo = Photo.find(params[:id])
    @photo.destroy

    redirect_to params[:redirect_path] || photos_path
  end

  private
    def photo_params
      params.require(:photo).permit(:title, :description, :mode, :image)
    end

end