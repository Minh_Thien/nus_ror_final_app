class ApplicationController < ActionController::Base

  # before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!, unless: :devise_controller?
  before_action :set_locale


  private
  # Overwriting the sign_out redirect path method
    def after_sign_out_path_for(resource_or_scope)
      new_user_session_path
    end
    def set_locale
      I18n.locale = params[:locale] || I18n.default_locale
    end

end
