class AlbumsController < ApplicationController

  def index
    if !current_user.admin
      @albums = current_user.albums.includes(:photos).page(params[:page])
    else
      @albums = Album.all.includes(:photos).page(params[:page])
    end
  end

  def new
    @album = Album.new
  end

  def edit
    @album = Album.find(params[:id])
  end

  def create
    @album = Album.new(album_params)
    @album.user = current_user
    if @album.save
      params[:photos][:image].each do |img|
        @photo = @album.photos.new(title:"Image created from album!", description: "No description", mode: "Private", image: img, user_id: current_user.id)
        if @photo.valid?
          @photo.save
        else
          flash[:alert] = @photo.errors.full_messages.join(',')
          return render :new
        end
      end
      redirect_to albums_path
    else
      render :new
    end
  end

  def show
    @album = Album.find(params[:id])
  end

  def update
    @album = Album.find(params[:id])
    if @album.update(album_params)
      if params[:photos]
        params[:photos][:image].each do |img|
          @photo = @album.photos.new(title:"Image created from album!", description: "No description", mode: "Private", image: img, user_id: current_user.id)
          if @photo.valid?
            @photo.save
          else
            flash[:alert] = @photo.errors.full_messages.join(',')
            return render :edit
          end
        end
      end
      redirect_to edit_album_path(params[:id])
    else
      render :edit
    end
  end

  def destroy
    @album = Album.find(params[:id])
    @album.destroy
    redirect_to albums_path
  end

  private
    def album_params
      params.require(:album).permit(:title, :description, :mode, photos_attributes: [:image, :user_id])
    end

end