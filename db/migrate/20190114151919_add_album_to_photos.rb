class AddAlbumToPhotos < ActiveRecord::Migration[5.2]
  def change
    add_reference :photos, :album, index: true
  end
end
