Rails.application.routes.draw do

  root to: 'feeds#index'

  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations', passwords: 'users/passwords' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :albums do
    resources :photos
  end
  resources :photos, :albums, :admins

end
